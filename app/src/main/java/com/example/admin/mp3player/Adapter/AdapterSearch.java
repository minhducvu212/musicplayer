package com.example.admin.mp3player.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.mp3player.Common.Album;
import com.example.admin.mp3player.Common.Artist;
import com.example.admin.mp3player.Common.Item;
import com.example.admin.mp3player.R;

import java.util.List;

import static com.example.admin.mp3player.Fragments.MusicFragment.mList;

/**
 * Created by minhd on 17/08/03.
 */

public class AdapterSearch extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int ITEM = 101;
    private final int ALBUM = 102;
    private final int ARTIST = 103 ;
    private final int TEXT = 104 ;
    private Context mContext ;
    private LayoutInflater layoutInflater ;
    private String path;
    List data ;

    public AdapterSearch(Context context , List list) {
        mContext = context ;
        layoutInflater= LayoutInflater.from(mContext) ;
        data = list ;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM) {
            Log.d("searchAAAAAA" , ITEM +"") ;
            return new SearchHolder (LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_music,parent, false) );
        }

        if (viewType == ALBUM) {
            Log.d("searchAAAAAA" , ALBUM +"ALBUM") ;

            return new AlbumHolder(LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_music,parent, false) );
        }

        if (viewType == ARTIST) {
            Log.d("searchAAAAAA" , ARTIST +"ARTIST") ;

            return new ArtistHolder(LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_artist,parent, false) );
        }
        if (viewType == TEXT) {
            Log.d("searchAAAAAA" , TEXT +"TEXT") ;

            return new TextHolder(layoutInflater.inflate(R.layout.item_text,parent, false)) ;
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case ITEM:
                SearchHolder searchHolder = (SearchHolder) holder ;
                searchHolder.tvName.setText(((Item) data.get(position)).getTitle());
                searchHolder.tvArtist.setText(((Item) data.get(position)).getArtist());
                if (getAlbumart(Uri.parse(((Item) data.get(position)).getPath())) != null) {
                    searchHolder.ivImage.setImageBitmap(getAlbumart(Uri.parse(((Item) data.get(position)).getPath())));
                }
                else {
                    searchHolder.ivImage.setImageResource(R.drawable.ivmusic);

                }
                break;

            case ALBUM:
                AlbumHolder albumHolder = (AlbumHolder) holder ;
                albumHolder.tvName.setText(((Album) data.get(position)).getNameAlbum());
                albumHolder.tvArtist.setText(((Album) data.get(position)).getArtist());
                for (int i = 0; i < mList.size(); i++) {
                    if ((mList.get(i).getAlbum()).equals(((Album) data.get(position)).getNameAlbum())){
                        path = mList.get(i).getPath() ;
                    }
                }
                if (getAlbumart(Uri.parse(path)) != null) {
                    albumHolder.ivImage.setImageBitmap(getAlbumart(Uri.parse(path)));
                }
                else {
                    albumHolder.ivImage.setImageResource(R.drawable.ivmusic);
                }
                break;

            case ARTIST:
                ArtistHolder artistHolder = (ArtistHolder) holder ;
                artistHolder.tvName.setText(((Artist) data.get(position)).getArtist());
                artistHolder.tvSongArtist.setText("Songs " + ((Artist) data.get(position)).getNumTrack());
                artistHolder.tvAlbumArtist.setText("Albums " + ((Artist) data.get(position)).getNumAl());

                for (int i = 0; i < mList.size(); i++) {
                    if ((mList.get(i).getArtist()).equals(((Artist) data.get(position)).getArtist())){
                        path = mList.get(i).getPath() ;
                    }
                }
                if (getAlbumart(Uri.parse(path)) != null) {
                    artistHolder.ivImage.setImageBitmap(getAlbumart(Uri.parse(path)));
                }
                else {
                    artistHolder.ivImage.setImageResource(R.drawable.ivmusic);
                }
                break;

            case TEXT:

                TextHolder textHolder = (TextHolder) holder ;
                textHolder.tvType.setText((String)data.get(position));
                break;
            default:
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof Item) {
            Log.d("searchAAAAAA", data.get(position) + "Item");
            return ITEM;
        }
        if (data.get(position) instanceof Album) {
            Log.d("searchAAAAAA", data.get(position) + "Album");

            return ALBUM;
        }
        if (data.get(position) instanceof Artist) {
            Log.d("searchAAAAAA", data.get(position) + "Artist");

            return ARTIST;
        }

        if (data.get(position) instanceof String) {
            Log.d("searchAAAAAA", data.get(position) + "Text");

            return TEXT;
        }

        return 0 ;

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class SearchHolder extends RecyclerView.ViewHolder{

        public TextView tvName , tvArtist ;
        public ImageView ivImage ;

        public SearchHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.name) ;
            tvArtist = (TextView) itemView.findViewById(R.id.artist) ;
            ivImage = (ImageView) itemView.findViewById(R.id.music) ;
        }
    }

    class AlbumHolder extends RecyclerView.ViewHolder{

        public TextView tvName , tvArtist ;
        public ImageView ivImage ;

        public AlbumHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.name) ;
            tvArtist = (TextView) itemView.findViewById(R.id.artist) ;
            ivImage = (ImageView) itemView.findViewById(R.id.music) ;
        }
    }

    class ArtistHolder extends RecyclerView.ViewHolder{

        public TextView tvName , tvSongArtist, tvAlbumArtist ;
        public ImageView ivImage ;

        public ArtistHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.nameArtist) ;
            tvSongArtist = (TextView) itemView.findViewById(R.id.tv_songArtist) ;
            tvAlbumArtist = (TextView) itemView.findViewById(R.id.tv_albumsSearch) ;

            ivImage = (ImageView) itemView.findViewById(R.id.musicArtist) ;
        }
    }

    class TextHolder extends RecyclerView.ViewHolder{

        public TextView tvType ;

        public TextHolder(View itemView) {
            super(itemView);
            tvType = (TextView) itemView.findViewById(R.id.tvType) ;

        }
    }

    public Bitmap getAlbumart(Uri uri) {

        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        byte[] rawArt;
        Bitmap art = null;
        BitmapFactory.Options bfo = new BitmapFactory.Options();

        mmr.setDataSource(mContext, uri);
        rawArt = mmr.getEmbeddedPicture();


        if (null != rawArt) {
            art = BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length, bfo);
        }
        return art;
    }
}
