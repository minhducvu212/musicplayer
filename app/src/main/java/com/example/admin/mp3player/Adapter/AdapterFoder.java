package com.example.admin.mp3player.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.admin.mp3player.R;

import static com.example.admin.mp3player.Fragments.ThuMucFragment.foders;

/**
 * Created by minhd on 17/08/03.
 */

public class AdapterFoder extends BaseAdapter {

    @Override
    public int getCount() {
        return foders.size();
    }

    @Override
    public Object getItem(int position) {
        return foders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_foder, parent, false);

        TextView tvFoder = (TextView) view.findViewById(R.id.tv_foder);
        TextView tvPath = (TextView) view.findViewById(R.id.tv_path);
        tvFoder.setText(foders.get(position).getName());
        tvPath.setText(foders.get(position).getPath());

        return view;
    }


}
