package com.example.admin.mp3player.Activity;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.admin.mp3player.Adapter.AdapterSearch;
import com.example.admin.mp3player.Common.Album;
import com.example.admin.mp3player.Common.Artist;
import com.example.admin.mp3player.Common.Item;
import com.example.admin.mp3player.R;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import io.reactivex.disposables.Disposable;

import static com.example.admin.mp3player.Fragments.MusicFragment.listAlbum;
import static com.example.admin.mp3player.Fragments.MusicFragment.listArtist;
import static com.example.admin.mp3player.Fragments.MusicFragment.mList;


public class SearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private RecyclerView rclSerch;
    private AdapterSearch adapter ;
    private Disposable disposableOrigin;
    public  List filteredList = new ArrayList<>();
    private SearchView searchView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.left48));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
        findViewByIds() ;

    }

    private void findViewByIds() {
        searchView = (SearchView) findViewById(R.id.searchView_searchFrag);
        rclSerch = (RecyclerView) findViewById(R.id.rcl_searchAct) ;
        searchView.setOnQueryTextListener(this);
        adapter = new AdapterSearch(this, filteredList) ;
        rclSerch.setLayoutManager(new LinearLayoutManager(this));
        rclSerch.setAdapter(adapter);

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true ;
    }

    private void filter(String query) {

        query = query.toLowerCase();

        filteredList.add(0, "Songs");
        for(Item item : mList){
            if ((convert(item.getTitle().toLowerCase())).contains(query)) {
                Log.d("TAGGNAME", convert(item.getTitle().toLowerCase()));

                filteredList.add(item);

            }
        }

        filteredList.add( "Albums");
        for(Album item : listAlbum){
            if ((convert(item.getNameAlbum().toLowerCase())).contains(query)) {

                Log.d("TAGGNAME", convert(item.getNameAlbum().toLowerCase()));

                filteredList.add(item);

            }
        }

        filteredList.add("Artist");
        for(Artist item : listArtist){
            if ((convert(item.getArtist().toLowerCase())).contains(query)) {
                Log.d("TAGGNAME", convert(item.getArtist().toLowerCase()));

                filteredList.add(item);

            }
        }
        Log.d("TAGGNAME", "========================");

        adapter.notifyDataSetChanged();
        Log.d("SIZE", filteredList.size() +"") ;

    }

    private String convert(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("").replaceAll("Đ", "D").replace("đ", "d");
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        filteredList.clear();

        if (disposableOrigin != null && !disposableOrigin.isDisposed()) {
            disposableOrigin.dispose();
        }
        String content = newText.toString().trim();
        filter(content) ;

        return true;
    }
}
