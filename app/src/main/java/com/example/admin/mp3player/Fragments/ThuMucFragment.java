package com.example.admin.mp3player.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.admin.mp3player.Adapter.AdapterFoder;
import com.example.admin.mp3player.Common.Foder;
import com.example.admin.mp3player.R;

import java.util.ArrayList;
import java.util.List;

import static com.example.admin.mp3player.Fragments.MusicFragment.mList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ThuMucFragment extends Fragment {


    private String content;
    private String foder;
    public  static List<Foder> foders = new ArrayList<>();
    private ListView lv ;
    private AdapterFoder adapterFoder ;

    public ThuMucFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getPath();
        View view = inflater.inflate(R.layout.fragment_thu_muc, container, false);
        lv = (ListView) view.findViewById(R.id.list_Foders) ;
        adapterFoder = new AdapterFoder() ;
        lv.setAdapter(adapterFoder);
        return view;
    }

    private void getPath() {
        for (int i = 0; i < mList.size(); i++) {
            content = mList.get(i).getPath();
            if (content.endsWith("MP3") | content.endsWith("mp3")) {
                for (int j = content.length() - 1; j >= 0; j--) {
                    if (content.charAt(j) == '/') {
                        content = content.substring(0, j );
                        foder = content.substring( content.lastIndexOf("/", j) + 1) ;
                        foders.add(new Foder(foder,content));
                        break;
                    }
                }
            }
            break;

        }

    }
    
}