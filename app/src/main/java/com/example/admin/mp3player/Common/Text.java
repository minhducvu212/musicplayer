package com.example.admin.mp3player.Common;

/**
 * Created by minhd on 17/08/03.
 */

public class Text {
    private String text ;

    public Text(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
