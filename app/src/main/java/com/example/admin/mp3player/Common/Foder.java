package com.example.admin.mp3player.Common;

/**
 * Created by minhd on 17/08/03.
 */

public class Foder {
    private String name ;
    private String path ;

    public Foder(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
