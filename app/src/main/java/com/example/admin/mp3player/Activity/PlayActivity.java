package com.example.admin.mp3player.Activity;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.admin.mp3player.Common.BlurBuilder;
import com.example.admin.mp3player.R;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.example.admin.mp3player.Activity.MainActivity.check;
import static com.example.admin.mp3player.Activity.MainActivity.checkPrev;
import static com.example.admin.mp3player.Activity.MainActivity.imBackground;
import static com.example.admin.mp3player.Activity.MainActivity.nextBoole;
import static com.example.admin.mp3player.Activity.MainActivity.number;
import static com.example.admin.mp3player.Activity.MainActivity.numberPrev;
import static com.example.admin.mp3player.Activity.MainActivity.prevBoole;
import static com.example.admin.mp3player.Activity.MainActivity.relayControl;
import static com.example.admin.mp3player.Activity.MainActivity.showingBackground;
import static com.example.admin.mp3player.Activity.MainActivity.showingPlay;
import static com.example.admin.mp3player.Activity.MainActivity.tvArtistOn;
import static com.example.admin.mp3player.Activity.MainActivity.tvNameArtist;
import static com.example.admin.mp3player.Activity.MainActivity.tvNameMusic;
import static com.example.admin.mp3player.Activity.MainActivity.tvNameOn;
import static com.example.admin.mp3player.Activity.MainActivity.tvShowingArtist;
import static com.example.admin.mp3player.Activity.MainActivity.tvShowingName;
import static com.example.admin.mp3player.Common.ServiceMedia.builder;
import static com.example.admin.mp3player.Common.ServiceMedia.manager;
import static com.example.admin.mp3player.Common.ServiceMedia.remoteViews;
import static com.example.admin.mp3player.Fragments.MusicFragment.adapterList;
import static com.example.admin.mp3player.Fragments.MusicFragment.artist;
import static com.example.admin.mp3player.Fragments.MusicFragment.bitmap;
import static com.example.admin.mp3player.Fragments.MusicFragment.mList;
import static com.example.admin.mp3player.Fragments.MusicFragment.mService;
import static com.example.admin.mp3player.Fragments.MusicFragment.mediaPlayer;
import static com.example.admin.mp3player.Fragments.MusicFragment.musicPlay;
import static com.example.admin.mp3player.Fragments.MusicFragment.name;
import static com.example.admin.mp3player.Fragments.MusicFragment.pos;
import static com.example.admin.mp3player.Fragments.MusicFragment.pos1;
import static com.example.admin.mp3player.Fragments.MusicFragment.posMusic;
import static com.example.admin.mp3player.Fragments.MusicFragment.position1;


public class PlayActivity extends AppCompatActivity implements View.OnClickListener,
        SeekBar.OnSeekBarChangeListener, MediaPlayer.OnBufferingUpdateListener {

    public static TextView tvNamePlay, tvArtistRun, tvRemainTime;
    public static ImageView prev, play, next, playImg, ivBackground;
    public static SeekBar seekBar;
    private int startTime;
    public static int finalTime;
    private Handler myHandler = new Handler();
    private int count = 0;
    public static RelativeLayout relayLayout;
    public static BlurBuilder blurBuilder = new BlurBuilder();
    private Bitmap mBit;
    private Bitmap bMap;
    private int position;
    public static boolean nextSeek = false;


    public PlayActivity() {
        // Required empty public constructor
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_play);
        relayControl.setVisibility(View.INVISIBLE);

        tvNamePlay = (TextView) findViewById(R.id.nameRun);
        tvArtistRun = (TextView) findViewById(R.id.artistRun);
        tvRemainTime = (TextView) findViewById(R.id.timeRemain);
        ivBackground = (ImageView) findViewById(R.id.iv_music_background);
        relayLayout = (RelativeLayout) findViewById(R.id.layoutBlur);
        prev = (ImageView) findViewById(R.id.prev);
        play = (ImageView) findViewById(R.id.play);
        next = (ImageView) findViewById(R.id.next);
        playImg = (ImageView) findViewById(R.id.imgPlay);


        position1 = posMusic;
        position = posMusic;

        finalTime = mService.getDuration();
        startTime = mService.getCurrentPosition();

        tvNamePlay.setText(name);
        tvArtistRun.setText(artist);

        if (bitmap != null) {
            playImg.setImageBitmap(bitmap);
            ivBackground.setImageBitmap(blurBuilder.blur(this, bitmap));

        } else {
            bMap = BitmapFactory.decodeResource(getResources(), R.drawable.ivmusic);
            ivBackground.setImageBitmap(blurBuilder.blur(this, bMap));
        }

        if (!mService.isPlaying()){
            showingPlay.setImageResource(R.drawable.icnotifplay);
            play.setImageResource(R.drawable.icnotifplay);
            Animation animation = AnimationUtils.loadAnimation(this,R.anim.rotate) ;
            playImg.startAnimation(animation);
            Log.d("Serviceeeeeeeeeee", " stop") ;
            mService.pause();
        }


//        if (nextSeek) {
//            tvNamePlay.setText(mList.get(posMusic + 1).getTitle());
//            tvArtistRun.setText(mList.get(posMusic + 1).getArtist());
//            Bitmap map1 = adapterList.getAlbumart(Uri.parse(mList.get(posMusic + 1).getPath()));
//            if (map1 != null) {
//                ivBackground.setImageBitmap(blurBuilder.blur(this,map1));
//                playImg.setImageBitmap(map1);
//            } else {
//                ivBackground.setImageResource(R.drawable.ivmusicbackground);
//            }
//        }

        if (nextBoole) {
            tvNamePlay.setText(mList.get(pos1).getTitle());
            tvArtistRun.setText(mList.get(pos1).getArtist());
            Bitmap map1 = adapterList.getAlbumart(Uri.parse(mList.get(pos1).getPath()));
            if (map1 != null) {
                ivBackground.setImageBitmap(map1);
            } else {
                ivBackground.setImageResource(R.drawable.ivmusicbackground);
            }
        }

        if (prevBoole) {
            tvNamePlay.setText(mList.get(pos).getTitle());
            tvArtistRun.setText(mList.get(pos).getArtist());
            Bitmap map1 = adapterList.getAlbumart(Uri.parse(mList.get(pos).getPath()));
            if (map1 != null) {
                ivBackground.setImageBitmap(map1);
            } else {
                ivBackground.setImageResource(R.drawable.ivmusicbackground);
            }
        }

        if (check) {
            inits(number);
        }
        if (checkPrev) {
            inits(numberPrev);
        }

        prev.setOnClickListener(this);
        play.setOnClickListener(this);
        next.setOnClickListener(this);

        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setClickable(false);
        seekBar.setMax(finalTime);
        seekBar.setProgress(mediaPlayer.getCurrentPosition());
        myHandler.postDelayed(UpdateSongTime, 100);
        seekBar.setOnSeekBarChangeListener(this);

        tvRemainTime.setText(String.format("%d min %d sec",
                TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                startTime)))
        );


        // seekBar.setOnSeekBarChangeListener(this);


//        if (num % 2 == 0) {
//            play.setImageResource(R.drawable.icnotifpause);
//        }
        nextSeek = true ;


    }

    private void inits(int number) {
        tvNamePlay.setText(mList.get(number).getTitle());
        tvArtistRun.setText(mList.get(number).getArtist());
        Bitmap bit = adapterList.getAlbumart(Uri.parse(mList.get(number).getPath()));

        if (bit != null) {
            ivBackground.setImageBitmap(blurBuilder.blur(this, bit));
            playImg.setImageBitmap(bit);
        } else {
            Bitmap Bit = BitmapFactory.decodeResource(getResources(), R.drawable.ivmusic);

            ivBackground.setImageBitmap(blurBuilder.blur(this, Bit));
            playImg.setImageResource(R.drawable.ivmusic);

        }
    }

    @Override
    public void onClick(View v) {

        count++;

        switch (v.getId()) {

            case R.id.prev:
                if (mService != null) {

                    if (position > 0) {
                        position = position - 1;
                    } else
                        position = 0;

                    setImagePlay(position);

                    try {
                        mService.play(position);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    mediaPlayer.setOnBufferingUpdateListener(this);
                    mService.start();
                    tvNamePlay.setText(mList.get(position).getTitle());
                    tvArtistRun.setText(mList.get(position).getArtist());
                    myHandler.postDelayed(UpdateSongTime, 100);
                    seekBar.setOnSeekBarChangeListener(this);
                    Bitmap bitmap = adapterList.getAlbumart(Uri.parse(mList.get(position).getPath()));
                    if (bitmap != null) {
                        playImg.setImageBitmap(bitmap);
                        ivBackground.setImageBitmap(blurBuilder.blur(this, bitmap));
                        remoteViews.setImageViewBitmap(R.id.notiImage, bitmap);

                    } else {
                        bMap = BitmapFactory.decodeResource(getResources(), R.drawable.ivmusic);
                        ivBackground.setImageBitmap(blurBuilder.blur(this, bMap));
                        remoteViews.setImageViewBitmap(R.id.notiImage, bMap);

                    }
                }
                break;

            case R.id.play:
                if (mService != null) {
                    if (mService.isPlaying()) {
                        play.setImageResource(R.drawable.icnotifplay);
                        showingPlay.setImageResource(R.drawable.icnotifplay);
                        remoteViews.setImageViewResource(R.id.btn_play, R.drawable.icnotifplay);
                        mService.pause();
                        manager.notify(101, builder.build());

                    } else {
                        play.setImageResource(R.drawable.icnotifpause);
                        showingPlay.setImageResource(R.drawable.icnotifpause);
                        remoteViews.setImageViewResource(R.id.btn_play, R.drawable.icnotifpause);
                        mService.start();
                        manager.notify(101, builder.build());

                    }
                }

                break;

            case R.id.next:

                if (mService != null) {

                    if (position1 < mList.size()) {
                        position1 = position1 + 1;
                    } else position1 = 0;

                    setImagePlay(position1);

                    try {
                        mService.play(position1);
                        mService.setOnBufferingUpdateListener();
                        tvNamePlay.setText(mList.get(position1).getTitle());
                        tvArtistRun.setText(mList.get(position1).getArtist());
                        tvNameOn.setText(mList.get(position1).getTitle());
                        tvArtistOn.setText(mList.get(position1).getArtist());
                        myHandler.postDelayed(UpdateSongTime, 100);
                        seekBar.setOnSeekBarChangeListener(this);
                        Bitmap bitmap = adapterList.getAlbumart(Uri.parse(mList.get(position1).getPath()));
                        if (bitmap != null) {
                            playImg.setImageBitmap(bitmap);
                            ivBackground.setImageBitmap(blurBuilder.blur(this, bitmap));
                            imBackground.setImageBitmap(blurBuilder.blur(this, bitmap));

                        } else {
                            bMap = BitmapFactory.decodeResource(getResources(), R.drawable.ivmusic);
                            ivBackground.setImageBitmap(blurBuilder.blur(this, bMap));
                        }

                    } catch (IndexOutOfBoundsException e) {
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                break;

            default:
                break;

        }

    }

    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            if (mService != null && mService.getCurrentPosition() <= mService.getDuration() - 1000) {
                startTime = mService.getCurrentPosition();

                tvRemainTime.setText(String.format("%d min %d sec",
                        TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                        TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                        toMinutes((long) startTime)))
                );
                seekBar.setProgress(mService.getCurrentPosition());
                Log.d("POSSITION", mService.getCurrentPosition() + "\t" + mService.getDuration());
                myHandler.postDelayed(this, 100);
            }
            else {
                nextSeek = false ;
                if (position1 < mList.size()) {
                    position1 = position1 + 1;
                } else position1 = 0;
                posMusic += 1 ;
                setImagePlay(position1);
                Log.d("AAAAAAAAAAAAAAAAAAAAAA", position1+"\t" + posMusic) ;

                try {
                    mService.play(position1);
                    mService.setOnBufferingUpdateListener();
                    tvNamePlay.setText(mList.get(position1).getTitle());
                    tvArtistRun.setText(mList.get(position1).getArtist());
                    tvShowingName.setText(mList.get(position1).getTitle());
                    tvShowingArtist.setText(mList.get(position1).getArtist());
                    tvNameMusic.setText(mList.get(position1).getTitle());
                    tvNameArtist.setText(mList.get(position1).getArtist());
                    tvNameOn.setText(mList.get(position1).getTitle());
                    tvArtistOn.setText(mList.get(position1).getArtist());
                    myHandler.postDelayed(UpdateSongTime, 100);
                    seekBar.setMax(mService.getDuration());
                    seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            if (fromUser) {
                                int time = progress;
                                if (time > mService.getDuration())
                                    time = mService.getDuration();

                                mService.pause();
                                mService.seek(time);
                                mService.start();
                                mService.setOnBufferingUpdateListener();


                            }
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }

                    });
                    Bitmap bitmap = adapterList.getAlbumart(Uri.parse(mList.get(position1).getPath()));
                    if (bitmap != null) {
                        playImg.setImageBitmap(bitmap);
                        ivBackground.setImageBitmap(blurBuilder.blur(getBaseContext(), bitmap));
                        musicPlay.setImageBitmap(bitmap);
                        showingBackground.setImageBitmap(blurBuilder.blur(getBaseContext(), bitmap));
                        imBackground.setImageBitmap(bitmap);
                    } else {
                        bMap = BitmapFactory.decodeResource(getResources(), R.drawable.ivmusic);
                        ivBackground.setImageBitmap(blurBuilder.blur(getBaseContext(), bMap));
                        imBackground.setImageBitmap(blurBuilder.blur(getBaseContext(), bMap));
                        playImg.setImageBitmap(bMap);
                        musicPlay.setImageBitmap(bMap);
                        showingBackground.setImageBitmap(blurBuilder.blur(getBaseContext(), bMap));

                    }

                } catch (Exception e) {
                }
            }
        }
    };


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            int time = progress;
            if (time > mService.getDuration())
                time = mService.getDuration();

            mService.pause();
            mService.seek(time);
            mService.start();
            mService.setOnBufferingUpdateListener();


        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        seekBar.setSecondaryProgress(percent);
    }

    private void setImagePlay(int position) {
        if (position < mList.size()) {

            try {
                mBit = adapterList.getAlbumart(Uri.parse(mList.get(position).getPath()));
            } catch (NullPointerException e) {

            }
            if (mBit != null) {
                playImg.setImageBitmap(mBit);
                musicPlay.setImageBitmap(mBit);
                relayLayout.setBackgroundDrawable(
                        new BitmapDrawable(blurBuilder.blur(this, mBit)));

            } else {
                playImg.setImageResource(R.drawable.ivmusic);
                relayLayout.setBackgroundResource(R.drawable.background);

            }

        }
    }

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        getView().setFocusableInTouchMode(true);
//        getView().requestFocus();
//        getView().setOnKeyListener( new View.OnKeyListener()
//        {
//            @Override
//            public boolean onKey( View v, int keyCode, KeyEvent event )
//            {
//                if( keyCode == KeyEvent.KEYCODE_BACK )
//                {
//                    getActivity().onBackPressed();
//                    relayControl.setVisibility(View.VISIBLE);
//                    return true;
//                }
//                return false;
//            }
//        } );
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        relayControl.setVisibility(View.VISIBLE);
        finish();
        Intent intent = new Intent(PlayActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
