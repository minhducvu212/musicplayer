package com.example.admin.mp3player.Common;

/**
 * Created by minhd on 17/08/03.
 */

public class Album {
    private String nameAlbum ;
    private String artist ;

    public Album(String nameAlbum, String artist) {
        this.nameAlbum = nameAlbum;
        this.artist = artist;
    }

    public String getNameAlbum() {
        return nameAlbum;
    }

    public void setNameAlbum(String nameAlbum) {
        this.nameAlbum = nameAlbum;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }
}
